"use strict";


/**
 * fetch les données des sondages depuis l'id du sujet
 * @param {int} id_sond
 */
function initSondage(id_sond) {
    fetch("/sondage/" + id_sond).then(sondageJson);
}

/**
 * Initialise la zone d'affichage des sondages
 */
function initSondages() {
    const div_sondage = document.getElementById("sondages");
    const titre = document.createElement("h3");
    titre.appendChild(document.createTextNode("Sondages"));
    div_sondage.appendChild(titre);
    const contenu = document.createElement("div");
    contenu.id = "sondages_contenu";
    div_sondage.appendChild(contenu);
}

/**
 * Vide la zone de sondages
 */
function clearSondages() {
    const div_sondage = document.getElementById("sondages_contenu");
    while (div_sondage.firstChild) {
        div_sondage.removeChild(div_sondage.firstChild);
    }
}

/**
 * transforme la promesse en json
 * @param {promesse} sondages_json_datas
 */
function sondageJson(sondages_json_datas) {
    sondages_json_datas.json().then(afficheSondage);
};


function afficheSondage(sondage) {
    const divSond = document.getElementById("sondages_contenu");
    const section = document.createElement('section');
    divSond.appendChild(section);
    let titre = document.createElement('h3');
    titre.innerText=sondage.question;
    section.appendChild(titre);
    const reponses = document.createElement('ul');
    for(const auteur in sondage.reponses) {
        const rep=document.createElement("li");
        rep.innerText = auteur + " : " + sondage.reponses[auteur];
        reponses.appendChild(rep);
    }
    section.appendChild(reponses);
}

initSondages();
